import { Component } from "@angular/core";
import { TraderTable } from "../view/trader-table";
import { Trader } from 'src/model/trader';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Automated Trading Platform";

  trader: Trader;

  handleSelectedTrader(trader: Trader) {
    this.trader = trader;
  }
}
