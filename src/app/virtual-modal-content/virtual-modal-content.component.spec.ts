import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualModalContentComponent } from './virtual-modal-content.component';

describe('VirtualModalContentComponent', () => {
  let component: VirtualModalContentComponent;
  let fixture: ComponentFixture<VirtualModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
