import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-virtual-modal-content',
  templateUrl: './virtual-modal-content.component.html',
  styleUrls: ['./virtual-modal-content.component.css']
})
export class VirtualModalContentComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  createTraderHandler() {
    this.activeModal.close();
  }

  dismissMe() {
    this.activeModal.dismiss('Cross click');
  }

}
