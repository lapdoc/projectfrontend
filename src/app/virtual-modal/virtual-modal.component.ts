import { Component, OnInit } from '@angular/core';
import {Trader} from "../../model/trader";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {VirtualModalContentComponent} from "../virtual-modal-content/virtual-modal-content.component";

@Component({
  selector: 'app-virtual-modal',
  templateUrl: './virtual-modal.component.html',
  styleUrls: ['./virtual-modal.component.css']
})
export class VirtualModalComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  open() {
    const modalRef = this.modalService.open(VirtualModalContentComponent,
      { windowClass: "ModalClass", centered: true, scrollable: false, size: 'lg'});
  }

}
