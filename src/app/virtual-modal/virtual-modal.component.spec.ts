import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualModalComponent } from './virtual-modal.component';

describe('VirtualModalComponent', () => {
  let component: VirtualModalComponent;
  let fixture: ComponentFixture<VirtualModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
