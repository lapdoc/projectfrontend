import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormModalContentComponent } from './form-modal-content.component';

describe('FormModalContentComponent', () => {
  let component: FormModalContentComponent;
  let fixture: ComponentFixture<FormModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
