import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form-modal-content',
  templateUrl: './form-modal-content.component.html',
  styleUrls: ['./form-modal-content.component.css']
})
export class FormModalContentComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  createTraderHandler() {
    this.activeModal.close();
  }

  dismissMe() {
    this.activeModal.dismiss('Cross click');
  }

}
