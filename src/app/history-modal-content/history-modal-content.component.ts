import { Component, OnInit, Input } from '@angular/core';
import { Trader } from 'src/model/trader';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {TraderService, TraderUpdate} from "../../model/trader-service";

@Component({
  selector: 'app-history-modal-content',
  templateUrl: './history-modal-content.component.html',
  styleUrls: ['./history-modal-content.component.css']
})
export class HistoryModalContentComponent implements OnInit, TraderUpdate {

  trader: Trader;
  service: TraderService;
  category: string;
  order: string;
  noOfItems = 70;
  traders: Array<Trader> = [];

  constructor(private activeModal: NgbActiveModal, service: TraderService) {
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  ngOnInit() {
  }

  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
    this.traders.forEach((item) => {
      if (item.ID === this.trader.ID) {
        this.trader = item;
      }
    });
  }

  dismissMe() {
    this.activeModal.dismiss('Cross click');
  }

  closeMe() {
    this.activeModal.close('Close click');
  }

}
