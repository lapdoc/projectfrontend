import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryModalContentComponent } from './history-modal-content.component';

describe('HistoryModalContentComponent', () => {
  let component: HistoryModalContentComponent;
  let fixture: ComponentFixture<HistoryModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
