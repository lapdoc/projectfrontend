import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ChartComponent} from "../chart/chart.component";
import {Trader} from "../../model/trader";

@Component({
  selector: 'app-chart-modal',
  templateUrl: './chart-modal.component.html',
  styleUrls: ['./chart-modal.component.css']
})
export class ChartModalComponent implements OnInit {

  @Input() selectedTrader: Trader;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  open() {
    const modalRef = this.modalService.open(ChartComponent, { centered: true, scrollable: false, size: 'xl'});
    modalRef.componentInstance.selectedTrader = this.selectedTrader;
  }

  getTrader(): boolean {
    return this.selectedTrader == null ? true : false;
  }

}
