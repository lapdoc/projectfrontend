import {Component, Input, OnInit} from '@angular/core';
import {Color, Label} from "ng2-charts";
import {ChartDataSets, ChartOptions} from "chart.js";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Trader} from "../../model/trader";
import {Position} from "../../model/position";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  lineChartData1: ChartDataSets[] = [{ data: [], label: 'Profit/Loss' }];
  lineChartLabels1: Label[] = [];
  lineChartData2: ChartDataSets[] = [{ data: [], label: 'ROI' }];
  lineChartLabels2: Label[] = [];

  // @ts-ignore
  lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: false,
  };
  lineChartColors1: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  lineChartColors2: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(36,179,29,0.3)',
    },
  ];
  lineChartLegend = true;
  lineChartType = 'line';
  lineChartPlugins = [];

  @Input() selectedTrader: Trader;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
    let myPositions: Position[] = [];
    if (this.selectedTrader.positions.length > 10) {
      myPositions = this.selectedTrader.positions.slice(this.selectedTrader.positions.length - 11, this.selectedTrader.positions.length - 2 );
    } else {
      myPositions = this.selectedTrader.positions.slice(0);
    }

    const data1: number[] = [];
    const data2: number[] = [];
    const label1: string[] = [];
    const label2: string[] = [];
    myPositions.forEach((pos: Position) => {
      data1.push(pos.profitOrLoss);
      // @ts-ignore
      data2.push(pos.roi);
      label1.push(pos.closingTrade.when);
      label2.push(pos.closingTrade.when);
    });

    this.lineChartData1[0].data = data1;
    this.lineChartData2[0].data = data2;
    this.lineChartLabels1 = label1;
    this.lineChartLabels2 = label2;


  }

  dismissMe() {
    this.activeModal.dismiss('Cross click');
  }

  closeMe() {
    this.activeModal.close('Close click');
  }

}
