import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';
import { TraderPipe } from 'src/model/trader-pipe';
import { ArraySortPipe } from 'src/model/array-sort-pipe';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faHistory } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faChartLine } from '@fortawesome/free-solid-svg-icons';
import { HistoryModalComponent } from './history-modal/history-modal.component';
import { HistoryModalContentComponent } from './history-modal-content/history-modal-content.component';
import { FormModalComponent } from './form-modal/form-modal.component';
import { FormModalContentComponent } from './form-modal-content/form-modal-content.component';
import { FilterPipe } from 'src/model/unique-pipe';
import { ChartsModule } from "ng2-charts";
import { ChartComponent } from './chart/chart.component';
import { ChartModalComponent } from './chart-modal/chart-modal.component';
import { VirtualModalContentComponent } from './virtual-modal-content/virtual-modal-content.component';
import { VirtualModalComponent } from './virtual-modal/virtual-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    TraderToolbar,
    TraderPipe,
    ArraySortPipe,
    HistoryModalComponent,
    HistoryModalContentComponent,
    FormModalComponent,
    FormModalContentComponent,
    FilterPipe,
    ChartComponent,
    ChartModalComponent,
    VirtualModalContentComponent,
    VirtualModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule,
    ChartsModule
  ],
  providers: [],
  entryComponents: [HistoryModalContentComponent, FormModalContentComponent, ChartComponent, VirtualModalContentComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(faArrowUp);
    library.add(faArrowDown);
    library.add(faCoffee);
    library.add(faHistory);
    library.add(faPlus);
    library.add(faChartLine);
  }
}
