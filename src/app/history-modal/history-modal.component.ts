import { Component, OnInit, Input } from '@angular/core';
import { Trader } from 'src/model/trader';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HistoryModalContentComponent } from '../history-modal-content/history-modal-content.component';

@Component({
  selector: 'app-history-modal',
  templateUrl: './history-modal.component.html',
  styleUrls: ['./history-modal.component.css']
})
export class HistoryModalComponent implements OnInit {

  @Input() trader: Trader;
  
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  open(trader: Trader) {
    const modalRef = this.modalService.open(HistoryModalContentComponent, {centered: true, scrollable: true, size: 'xl'});
    modalRef.componentInstance.trader = trader;
  }

  getTrader(): boolean {
    return this.trader == null ? true : false;
  }

}
