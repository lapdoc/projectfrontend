import { Trader } from "../model/trader";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";
import { Component, Output, EventEmitter } from '@angular/core';


/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"],
})
export class TraderTable implements TraderUpdate {

  service: TraderService;
  traders: Array<Trader> = [];
  display = false;
  ID:number;
  op:string=null;
  selectedTrader: Trader;
  traderState = new Map();
  whichStock2MA:string='all';
  PorL2MA:string='all';
  state2MA:string='all';
  whichStockBB:string='all';
  PorLBB:string='all';
  stateBB:string='all';
  filteredTraders2MA:Array<Trader>=[];
  filteredTradersBB:Array<Trader>=[];


  @Output() selectedTraderEvent = new EventEmitter();

  /**
   * Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService) {
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  

  // filter traders tables
  filterTraders2MA(){
    this.filteredTraders2MA=this.traders;

    if (this.whichStock2MA!='all') {
      this.filteredTraders2MA=this.filterByStock(this.whichStock2MA,this.filteredTraders2MA);
    }
    if (this.PorL2MA!='all') {
      this.filteredTraders2MA=this.filterByProfit(this.PorL2MA, this.filteredTraders2MA);
    }
    if (this.state2MA!='all'){
      this.filteredTraders2MA=this.filterByState(this.state2MA, this.filteredTraders2MA);
    }
  }

  filterTradersBB(){
    this.filteredTradersBB=this.traders;

    if (this.whichStockBB!='all') {
      this.filteredTradersBB=this.filterByStock(this.whichStockBB,this.filteredTradersBB);
    }
    if (this.PorLBB!='all') {
      this.filteredTradersBB=this.filterByProfit(this.PorLBB, this.filteredTradersBB);
    }
    if (this.stateBB!='all'){
      this.filteredTradersBB=this.filterByState(this.stateBB, this.filteredTradersBB);
    }
  }


  filterByStock(whichStock:string, filteredTraders:Array<Trader>): Array<Trader>{
    let newfilteredTraders:Trader[]=[];
    for (let t of filteredTraders) {
      if (t.stock==whichStock) {newfilteredTraders.push(t);}
    }
    return newfilteredTraders;
  }

  filterByProfit(PorL:string, filteredTraders:Array<Trader>): Array<Trader>{
    let newfilteredTraders:Trader[]=[]; 
    for (let t of filteredTraders) {
      if (PorL=='profit' && t.profitOrLoss>0) {newfilteredTraders.push(t);}
      else if (PorL=='loss' && t.profitOrLoss<0) {newfilteredTraders.push(t);}
    }
    return newfilteredTraders;
  }

  filterByState(state:string, filteredTraders:Array<Trader>): Array<Trader>{
    let newfilteredTraders:Trader[]=[];
    for (let t of filteredTraders) {
      if (this.getState(t)==state) {newfilteredTraders.push(t);}
    }
    return newfilteredTraders;
  }


  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
    this.traders.forEach((trader) => {
      if (this.selectedTrader != null) {
        document.getElementById(`${this.selectedTrader["@type"]}${this.selectedTrader.ID}`).classList.add('selected');
        this.selectedTraderEvent.emit(this.selectedTrader);
      }
    });
    this.filterTraders2MA();
    this.filterTradersBB();
    this.selectedTraderEvent.emit(this.selectedTrader);
  }

  getTraderById(id: number): Trader {
    for(let i=0; i<this.traders.length;i++) {
      if(this.traders[i].ID === id) {
        return this.traders[i];
      }
    }
  }

  selectTrader(type: string, id: number) {
    if (this.selectedTrader != null) {
      document.getElementById(`${this.selectedTrader["@type"]}${this.selectedTrader.ID}`).classList.remove('selected');
      if (this.selectedTrader.ID === id) {
        this.selectedTrader = null;
        this.selectedTraderEvent.emit(this.selectedTrader);
        return;
      }
    }

    const trader = this.getTraderById(id);
    this.selectedTrader = trader;
    this.selectedTraderEvent.emit(this.selectedTrader);
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    if (trader.stopping) {
      return "Stopping";
    } else if (trader.active) {
      return "Started";
    } else if (!trader.active) {
      return "Stopped";
    }
  }

  getClass(state: string) {
    if (state === 'Started') {
      return 'btn btn-success btn-sm';
    } else {
      return 'btn btn-danger btn-sm';
    }
  }


  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(type: string): number {
    let totalTrades = 0;
    this.traders.forEach((trader) => {
      if (trader["@type"] === type) {
        totalTrades += trader.trades;
      }
    });

    return totalTrades;
    // return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(type: string): number {
    let totalProfit = 0;
    this.traders.forEach((trader) => {
      if (trader["@type"] === type) {
        totalProfit += trader.profitOrLoss;
      }
    });

    return totalProfit;
    // return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  getProfit(): Array<number> {
    return this.traders[0].positions.map(p=>p.profitOrLoss);
  }

  getOpenTrade(): Array<number> {
    let trader = this.traders[0]
    let my : number[];
    return my;

  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(ev: any, hardStop: boolean) {
    const index = parseInt(ev.target.id, 10);
    const trader = this.getTraderById(index);
    this.service.setActive(trader.ID, !trader.active);
  }

  show_details(id:number) {
    this.display=!this.display;
    this.ID=id;
  }

  getROI(trader:Trader): string {
    if (isNaN(trader.ROI)) {
      return '0';
    } else {
      return trader.ROI.toFixed(3);
    }
  }

}
