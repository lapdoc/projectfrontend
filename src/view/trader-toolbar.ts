import { Component, Output, EventEmitter } from "@angular/core";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { BollingerBand } from 'src/model/bollinger-band';

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"]
})
export class TraderToolbar {

  service: TraderService;
  type: string;
  stock: string;
  size: number;
  lengthShort: number;
  lengthLong: number;
  windowLength: number;
  multiplier: number;
  exitThreshold: number;
  errorMsg: string;
  real = false;


  @Output() createdTrader = new EventEmitter();

  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(service: TraderService) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.lengthShort = 2;
    this.lengthLong = 4;
    this.windowLength = 3;
    this.multiplier = 1.5;
    this.exitThreshold = 3;
  }

  checkValid() {
    if (this.lengthShort >= this.lengthLong) {
      this.errorMsg = "Short must be less than Long";
      return true;
    } else if (this.hasNumber(this.stock)) {
      this.errorMsg = "Stock name must not contain digits";
      return true;
    } else if (this.size <= 0) {
      this.errorMsg = "Stock size has to be a non zero value";
      return true;
    } else {
      return false;
    }
  }

  hasNumber(myString: string) {
    return /\d/.test(myString);
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    if (this.type === '2MA') {
      this.service.createTrader(new TwoMovingAverages
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.lengthShort * 1000 * 15, this.lengthLong * 1000 * 15, this.exitThreshold / 100, !this.real),
        !this.real ? "true" : "false");
    } else if (this.type === 'BB') {
      this.service.createTrader(new BollingerBand
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.windowLength * 1000 * 15, this.multiplier, this.exitThreshold / 100, !this.real),
        !this.real ? "true" : "false");
    }
    this.createdTrader.emit();
  }
}
