import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a 2MA strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
export class BollingerBand extends Trader {
  windowLength: number;
  multiplier: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      windowLength: number, multiplier: number,  exitThreshold: number, real: boolean) {
    super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI, real);
    this.windowLength = windowLength;
    this.multiplier = multiplier;
    this.exitThreshold = exitThreshold;
  }
}
