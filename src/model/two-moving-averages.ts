import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a 2MA strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
export class TwoMovingAverages extends Trader {
  lengthShort: number;
  lengthLong: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      lengthShort: number, lengthLong: number,  exitThreshold: number, real: boolean) {
    super("2MA", ID, stock, size, active, stopping, positions, profitOrLoss, ROI, real);
    this.lengthShort = lengthShort;
    this.lengthLong = lengthLong;
    this.exitThreshold = exitThreshold;
  }
}
