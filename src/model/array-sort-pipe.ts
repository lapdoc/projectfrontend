import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "sort"
  })
  export class ArraySortPipe  implements PipeTransform {
    ord:string=null;
  
    transform(array: any, category: string, order:string, items: number): any[] {
      if (!Array.isArray(array)) {
        return;
      }
      if (order === 'ascending' && category === 'size') {
        array.sort((a: any, b: any) => {
          if (a.openingTrade[category] < b.openingTrade[category]) {
            return -1;
          } else if (a.openingTrade[category] > b.openingTrade[category]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
      if (order === 'ascending' && category === 'openTime') {
        category = 'when';
        array.sort((a: any, b: any) => {
          if (a.openingTrade[category] < b.openingTrade[category]) {
            return -1;
          } else if (a.openingTrade[category] > b.openingTrade[category]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
      if(order === 'descending' && category === 'openTime') {
        category = 'when';
        array.sort((a: any, b: any) => {
          if (a.openingTrade[category] < b.openingTrade[category]) {
            return 1;
          } else if (a.openingTrade[category] > b.openingTrade[category]) {
            return -1;
          } else {
            return 0;
          }
        });
      }
      if (order === 'ascending' && category === 'closeTime') {
        category = 'when';
        array.sort((a: any, b: any) => {
          if (a.closingTrade[category] < b.closingTrade[category]) {
            return -1;
          } else if (a.closingTrade[category] > b.closingTrade[category]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
      if(order === 'descending' && category === 'closeTime') {
        category = 'when';
        array.sort((a: any, b: any) => {
          if (a.closingTrade[category] < b.closingTrade[category]) {
            return 1;
          } else if (a.closingTrade[category] > b.closingTrade[category]) {
            return -1;
          } else {
            return 0;
          }
        });
      }
      if(order === 'descending' && category === 'size') {
        array.sort((a: any, b: any) => {
          if (a.openingTrade[category] < b.openingTrade[category]) {
            return 1;
          } else if (a.openingTrade[category] > b.openingTrade[category]) {
            return -1;
          } else {
            return 0;
          }
        });
      }
      if (order == 'ascending') {
        array.sort((a: any, b: any) => {
          if (a[category] < b[category]) {
            return -1;
          } else if (a[category] > b[category]) {
            return 1;
          } else {
            return 0;
          }
        });
      } 
      if(order=='descending') {
        array.sort((a: any, b: any) => {
          if (a[category] < b[category]) {
            return 1;
          } else if (a[category] > b[category]) {
            return -1;
          } else {
            return 0;
          }
        });
      } 
  
      return array.slice(0, items);
    }
  }
  