import { Injectable } from "@angular/core";
import { Trader } from "./trader";
import { TwoMovingAverages } from "./two-moving-averages";
import { BollingerBand } from './bollinger-band';

/**
 * Subscriber interface for receiving updates on current traders.
 *
 * @author Will Provost
 */
export interface TraderUpdate {
  latestTraders(traders: Array<Trader>);
}

/**
 * A client component for the HFTrader application's TraderService.
 * Also has the responsibility to set a timer and to poll the service
 * for updates on traders and their status and trading history.
 * Subscribe with an implementation of the above TraderUpdate interface,
 * and this component will push updates as it fetches them.
 *
 * @author Will Provost
 */
@Injectable({
 providedIn: 'root',
})
export class TraderService {

  URL: string;
  subscribers: Array<TraderUpdate>;
  timer: any;

  /**
   * Set up service URL. Bind all methods so our 'this' references
   * make sense. Initialize subscribers array. Start the polling timer.
   */
  constructor() {
    this.URL = "/traders";

    this.handler = this.handler.bind(this);
    this.checkResponseCode = this.checkResponseCode.bind(this);
    this.checkOK = this.checkOK.bind(this);
    this.checkCreated = this.checkCreated.bind(this);
    this.getTraders = this.getTraders.bind(this);
    this.getTrader = this.getTrader.bind(this);
    this.setActive = this.setActive.bind(this);
    this.createTrader = this.createTrader.bind(this);

    this.subscribers = [];
    this.start();
  }

  /**
   * Add the subscriber.
   */
  subscribe(subscriber: TraderUpdate) {
    this.subscribers.push(subscriber);
  }

  /**
   * Remove the subscriber.
   */
  unsubscribe(subscriber: TraderUpdate) {
    for (let i = 0; i < this.subscribers.length; ++i) {
      if (this.subscribers[i] === subscriber) {
        this.subscribers.splice(i, 1);
        break;
      }
    }
  }

  /**
   * Call each subscriber, passing the current traders array.
   */
  notify() {
    this.getTraders().then(traders => {
        for (const subscriber of this.subscribers) {
          subscriber.latestTraders(traders);
        }
      });
  }

  /**
   * Set a timer to go off in one second and then every 15 seconds therafter,
   * calling our notify() method each time.
   */
  start() {
    const oneSecond = setInterval(() => {
        this.notify();
        this.timer = setInterval(this.notify.bind(this), 15000);
        clearInterval(oneSecond);
      }, 1000);
  }

  /**
   * Shut down the polling timer.
   */
  stop() {
    clearInterval(this.timer);
  }

  /**
   * Error handler currently just logs to the console.
   * A popup message box or some other UI should come into play at some point.
   */
  handler(err): any {
    console.log(err);
    return null;
  }

  /**
   * Helper to check that the HTTP response code was the expected alue.
   * Either throws an error or returns the response, making the function
   * suitable for use in a fetch/then chain.
   */
  checkResponseCode(response: Response, expected: number): Response {
    if (response.status !== expected) {
      throw Error("Unexpected response code: " + response.status);
    }
    return response;
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 200 OK.
   */
  checkOK(response: Response): Response {
    return this.checkResponseCode(response, 200);
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 201 Created.
   */
  checkCreated(response: Response): Response {
    return this.checkResponseCode(response, 201);
  }

  /**
   * Parses the given (weakly typed) object and creates the appropriate
   * type of Trader, holding the appropriate values.
   * Currently, only 2MA traders are supported.
   */
  static makeTrader(source: any): Trader {
    if (source["@type"] === "2MA") {
      return new TwoMovingAverages(source.id, source.stock, source.size,
        source.active, source.stopping,
        source.positions, source.profitOrLoss, source.roi,
        source.lengthShort, source.lengthLong, source.exitThreshold, source.real);
    } else if (source["@type"] === "BB") {
      return new BollingerBand(source.id, source.stock, source.size,
        source.active, source.stopping,
        source.positions, source.profitOrLoss, source.roi,
        source.windowLength, source.multiplier, source.exitThreshold, source.real);
    } else {
      throw Error("Unknown trader type: " + source["@type"]);
    }
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing an array of
   * Trader objects.
   */
  getTraders(): Promise<Array<Trader>> {
    return fetch(this.URL)
      .then(this.checkOK)
      .then(response => response.json())
      .then(traders => traders.map(TraderService.makeTrader))
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing the
   * requested trader object.
   */
  getTrader(ID: number): Promise<Trader> {
    return fetch(this.URL + "/" + ID)
      .then(this.checkOK)
      .then(response => response.json())
      .then(TraderService.makeTrader)
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation, and on successful response triggers
   * a notify() which in turn will fetch an updated list of traders.
   */
  setActive(ID: number, start: boolean) {
    return fetch(this.URL + "/" + ID + "/active", {
          method: "PUT",
          body: start ? "true" : "false"
        })
      .then(this.checkOK)
      .then(response => { this.notify(); return response; })
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing
   * the newly created trader, which will carry the server-generated ID.
   */
  createTrader(trader: Trader, real: string): Promise<Trader> {
    return fetch(`${this.URL}/${real}`, {
          method: "POST",
          headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
          body: JSON.stringify(trader)
        })
      .then(this.checkCreated)
      .then(response => response.json())
      .then(TraderService.makeTrader)
      .then(created => { this.notify(); return created; })
      .catch(this.handler);
  }
}
