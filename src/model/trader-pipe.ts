import { Pipe, PipeTransform } from '@angular/core';
import { Trader } from './trader';

@Pipe({
    name: 'TraderFilter',
    pure: false
})
export class TraderPipe implements PipeTransform {
    transform(items: Trader[], filter: string): Trader[] {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(item => item["@type"] === filter);
    }
}