import { Pipe, PipeTransform } from '@angular/core';
import { Trader } from './trader';

@Pipe({
    name:'unique'
})

export class FilterPipe implements PipeTransform
{

  transform(items: Trader[], filter: string): any {

    // Remove the duplicate elements
    let uniqueArray:Trader[]=[];
    let uniqueStock:string[]=[];
    for (let i of items) {
        if (!uniqueStock.includes(i.stock)) {
            uniqueStock.push(i.stock);
            uniqueArray.push(i);
        }
    }

  return uniqueArray;   } 
}